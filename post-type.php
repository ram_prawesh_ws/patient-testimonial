<?php
	function create_patient_testimonial() {
		$labels = array(
			'name' 					=> __( 'Testimonials', 'patient_testimonials'),
			'singular_name' 		=> __( 'Testimonial', 'patient_testimonials' ),
			'add_new' 				=> __( 'Add Testimonial', 'patient_testimonials' ),
			'add_new_item' 			=> __( 'Add New Testimonial', 'patient_testimonials' ),
			'edit' 					=> __( 'Edit', 'patient_testimonials' ),
			'edit_item' 			=> __( 'Edit Testimonial', 'patient_testimonials' ),
			'new_item' 				=> __( 'New Testimonial', 'patient_testimonials' ),
			'view' 					=> __( 'View Testimonial', 'patient_testimonials' ),
			'view_item' 			=> __( 'View Testimonial', 'patient_testimonials' ),
			'search_items' 			=> __( 'Search Testimonial', 'patient_testimonials' ),
			'not_found' 			=> __( 'No Testimonial found', 'patient_testimonials' ),
			'not_found_in_trash' 	=> __( 'No Testimonial found in Trash', 'patient_testimonials' ),
			'parent' 				=> __( 'Parent Testimonial', 'patient_testimonials' ),
		);
		$capabilities = array(
			'create_posts' 			=> 'create_posts',//'create_testimonial',
			'edit_posts' 			=> 'edit_posts', //'edit_testimonial',
			'edit_others_posts' 	=> 'edit_others_posts', //'edit_others_testimonial',
			'publish_posts' 		=> 'publish_posts', //'publish_testimonial',
			'read_post' 			=> 'read_post', //'read_testimonial',
			'read_private_posts' 	=> 'read_private_posts', //'read_private_testimonial',
			'delete_post' 			=> 'delete_post' //'delete_testimonial',
			);
		$args = array(
			'labels'				=> $labels,
			'description' 			=> __( 'A Testimonials is a type of content that is registerd for enter patient feedback.', 'patient_testimonials' ),
			'public' 				=> true,
			'has_archive'			=> true,
			'show_ui' 				=> true,
			'show_in_menu' 			=> false,
			'publicly_queryable'	=> true,
			'exclude_from_search' 	=> false,
			//'menu_position' 		=> 10,
			//'menu_icon' 			=> get_stylesheet_directory_uri() . '/images/cube.png',
			'hierarchical' 			=> true,
			'query_var' 			=> true,
			//'capability_type' 		=> 'patient_testimonial',
			//'capabilities' 			=> $capabilities,
			'map_meta_cap'			=> true,
			'supports' 				=> array( 'title', 'author', 'thumbnail', 'excerpt', 'revisions', 'editor' ),
			'taxonomies' 			=> array( 'disease-categories'),
			'can_export' 			=> true,
		);
		register_post_type( 'testimonial', $args );
	}
	add_action( 'init', 'create_patient_testimonial' );
	add_action( 'init', 'create_diseas_categories');
	function create_diseas_categories()
	{
		$labels = array(
			'name'              	=> _x( 'Disease Categories', 'taxonomy general name', 'patient_testimonials' ),
			'singular_name'     	=> _x( 'Disease Category', 'taxonomy singular name', 'patient_testimonials' ),
			'search_items'      	=> __( 'Search Disease Categories', 'patient_testimonials' ),
			'all_items'         	=> __( 'All Disease Categories', 'patient_testimonials' ),
			'parent_item'       	=> __( 'Parent Disease Category', 'patient_testimonials' ),
			'parent_item_colon' 	=> __( 'Parent Disease Category:', 'patient_testimonials' ),
			'edit_item'         	=> __( 'Edit Disease Category', 'patient_testimonials' ),
			'update_item'       	=> __( 'Update Disease Category', 'patient_testimonials' ),
			'add_new_item'      	=> __( 'Add New Disease Category', 'patient_testimonials' ),
			'new_item_name'     	=> __( 'New Disease Category', 'patient_testimonials' ),
			'menu_name'         	=> __( 'Disease Categories', 'patient_testimonials' ),
		);
		$taxcapabilities = array(
		    'assign_terms' 			=> 'edit_posts',
			'edit_terms' 			=> 'edit_posts',
		    'manage_terms' 			=> 'edit_posts'
		);
		$args = array(
			'hierarchical'      	=> true,
			'labels'            	=> $labels,
			'show_ui'           	=> true,
			'show_admin_column' 	=> true,
			'query_var'         	=> true,
			'capability_type' 		=> 'cube',
			'capabilities' 			=> $taxcapabilities,
			'map_meta_cap'			=> true,
			'rewrite'           	=> array( 'slug' => 'disease-categories' ),
		);

		register_taxonomy( 'disease-categories', array('testimonial'), $args );
	}
	
	add_filter( 'manage_edit-testimonial_columns', 'edit_testimonial_columns' ) ;
	function edit_testimonial_columns( $columns ) { //print_r($columns); exit();
		$columns = array(
			'cb' 							=> '<input type="checkbox" />',
			'coverimage'					=> __( 'Cover Image' ),
			'title' 						=> __( 'Patient Name' ),
			'taxonomy-disease-categories' 	=> __( 'Disease Categories' ),
			'author'						=> __( 'Author' ),
			'date' 							=> __( 'Date' )
		);
		return $columns;
	}

	add_action( 'manage_testimonial_posts_custom_column', 'manage_testimonial_columns', 10, 2 );
	function manage_testimonial_columns( $column, $post_id ) {
		global $post;
		switch( $column ) {

			case 'coverimage' :
				$image_url = wp_get_attachment_image_src(get_post_thumbnail_id( $post_id ), 'thumbnail');
				$image_url = $image_url[0];
				if ( empty( $image_url ) )
					echo __( 'No Image' );
				else
					echo '<img src="'.$image_url.'" class="cover_image" alt="" />';

				break;
			/*case 'taxonomy-disease-categories' :
				$terms = get_the_terms( $post_id, 'disease-categories' );
				if ( !empty( $terms ) ) {
					$out = array();
					foreach ( $terms as $term ) {
						$out[] = sprintf( '<a href="%s">%s</a>',
							esc_url( add_query_arg( array( 'post_type' => $post->post_type, 'taxonomy-disease-categories' => $term->slug ), 'edit.php' ) ),
							esc_html( sanitize_term_field( 'name', $term->name, $term->term_id, 'Disease Categories', 'display' ) )
						);
					}
					echo join( ', ', $out );
				} else {
					_e( 'No Diseas Selected' );
				}
				break;*/
			default :
				break;
		}
	}

	//add_filter( 'manage_edit-testimonial_sortable_columns', 'testimonial_sortable_columns' );
	function testimonial_sortable_columns( $columns ) {
		$columns['coverimage'] = 'coverimage';
		return $columns;
	}

	//add_action( 'load-edit.php', 'edit_testimonial_load' );
	function edit_testimonial_load() {
		add_filter( 'request', 'testimonial_sort_coverimage' );
	}

	function testimonial_sort_coverimage( $vars ) {
		if ( isset( $vars['post_type'] ) && 'testimonial' == $vars['post_type'] ) {
			if ( isset( $vars['orderby'] ) && 'coverimage' == $vars['orderby'] ) {
				/* Merge the query vars with our custom variables. */
				$vars = array_merge(
					$vars,
					array(
						'meta_key' => 'coverimage',
						'orderby' => 'meta_value_num'
					)
				);
			}
		}
		return $vars;
	}

	function testimonial_change_title_text( $title ){
    	$screen = get_current_screen();
     	if  ( 'testimonial' == $screen->post_type ) {
        	$title = 'Enter patient name';
     	}
     	return $title;
	}
	add_filter( 'enter_title_here', 'testimonial_change_title_text' );

	function testimonial_featured_image_metabox_title() {
		remove_meta_box( 'postimagediv', 'testimonial', 'side' );
		add_meta_box( 'postimagediv', __( 'Cover Image', 'patient_testimonials' ), 'post_thumbnail_meta_box', 'testimonial', 'side' );
	}
	add_action('do_meta_boxes', 'testimonial_featured_image_metabox_title' );
	/*
	 * Change the featured image metabox link text
	 *
	 * @param  string $content Featured image link text
	 * @return string $content Featured image link text, filtered
	 */
	function testimonial_featured_image_text( $content ) {
		if ( 'testimonial' === get_post_type() ) {
			$content = str_replace( 'Set featured image', __( 'Set Cover Image', 'patient_testimonials' ), $content );
			$content = str_replace( 'Remove featured image', __( 'Remove Cover Image', 'patient_testimonials' ), $content );
		}
		return $content;
	}
	add_filter( 'admin_post_thumbnail_html', 'testimonial_featured_image_text' );

	//add_filter( 'manage_edit-testimonial_columns', 'testimonial_edit_testimonial_change_title_in_list' );
	function testimonial_edit_testimonial_change_title_in_list($columns) {
	    $columns[ 'title' ] = 'Patient Name';  
    	return $columns;
	}

	add_filter('single_template', 'testimonial_custom_single_template');
	function testimonial_custom_single_template($single) {
	    global $wp_query, $post; 
	    if ( $post->post_type == 'testimonial' ) { 
	        if ( file_exists( dirname( __FILE__ ) . '/templates/single-testimonial.php' ) ) {
	            return dirname( __FILE__ ) . '/templates/single-testimonial.php';
	        }
	    }
	    return $single;
	}
	add_filter('archive_template', 'testimonial_custom_archive_template');
	function testimonial_custom_archive_template($single) {
	    global $wp_query, $post;
	    if ( $post->post_type == 'testimonial' ) {
	        if ( file_exists( dirname( __FILE__ ) . '/templates/archive-testimonial.php' ) ) {
	            return dirname( __FILE__ ) . '/templates/archive-testimonial.php';
	        }
	    }
	    return $single;
	}
	add_filter('taxonomy_template', 'testimonial_custom_taxonomy_template');
	function testimonial_custom_taxonomy_template($single) {
	    global $wp_query, $post;
	    if ( $post->post_type == 'testimonial' ) {
	        if ( file_exists( dirname( __FILE__ ) . '/templates/taxonomy-disease-categories.php' ) ) {
	            return dirname( __FILE__ ) . '/templates/taxonomy-disease-categories.php';
	        }
	    }
	    return $single;
	}
	add_filter('next_posts_link_attributes', 'posts_link_attributes_next');
	add_filter('previous_posts_link_attributes', 'posts_link_attributes_back');

	function posts_link_attributes_next() {
	    return 'class="btn rightArrowBtn"';
	}
	function posts_link_attributes_back() {
	    return 'class="btn leftArrowBtn"';
	}
	function testimonial_queries( $query ) {
		$options = get_option('patient_testimonials_options');
		$no = $options['patient_testimonials_field_page_row']+$options['patient_testimonials_field_page_column'];
	    if(is_post_type_archive('testimonial')){
	      	$query->set('posts_per_page', $no);
	    }
	    if (taxonomy_exists( 'disease-categories' )) {
	    	$query->set('posts_per_page', $no);
	    }
	}
	add_action( 'pre_get_posts', 'testimonial_queries' );
?>