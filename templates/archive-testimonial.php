<?php
/**
 * Testimonial landing template file
 */
$options = get_option('patient_testimonials_options');
get_header(); ?>
<style type="text/css">
	
	body{
		font-family: 'Open Sans',
		sans-serif;
		font-weight: <?php echo $options['patient_testimonials_field_font_weight']; ?>;
		background-color: <?php echo $options['patient_testimonials_field_bg_color']; ?>;
		/*color:#838383;*/
	}
	<?php if(empty($options['patient_testimonials_field_bg_color'])){
		echo 'body{background: url('.$options['patient_testimonials_field_bg_image'].') repeat-x, rgb(93,198,255)';
	}?>
	

</style>
      <link href="<?php echo plugin_dir_url( __FILE__ ); ?>/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">

      <link href="<?php echo plugin_dir_url( __FILE__ ); ?>/css/screen.css" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800" rel="stylesheet">
	<body>
      	<div class="main-sec">
         	<div class="custom-container">
            	<div class="top-section">
               		<h2 class="select-heading">Select Disease Condition</h2>
	               	<form>
	                  	<select class="select-option" onchange="location = this.value;">

	                  	<?php
	                  		echo '<option value="'.site_url('/testimonial/').'">All</option>';
					  		$args = array('taxonomy' => 'disease-categories', 'orderby' => 'name', 'parent' => 0,  );
							$categories = get_terms( $args );
							foreach ( $categories as $category ) {
								$selected = '';
							echo '<option '.$selected.' value="'.site_url('/disease-categories/'.$category->slug).'">' . $category->name . '</option>';
						} ?>
						</select>
	               </form>
	               <div class="top-btn">
	               		<?php if( get_previous_posts_link() ) : previous_posts_link( 'Back' ); endif; ?>
	               		<?php if( get_next_posts_link() ) : next_posts_link( 'See More'); endif; ?>
	                  	<?php //posts_nav_link( '', '', 'Back' ); ?>
						<?php //posts_nav_link( '', '', 'See More' ); ?>
	               </div>
            	</div>
            	
               <?php  global $query_string;
                  //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                  //query_posts($query_string."paged=$paged&posts_per_page=1");?>
               <?php while ( have_posts() ) : the_post(); ?>
               	<div class="clearfix"></div>
               <div class="col-6">
                  	<div class="main-content">
                     	<div class="client-img">
                           <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
                        	<img src="<?php echo $img[0]; ?>">   
                     	</div>
	                    <div class="inn-client-details">
	                        <h3 class="client-name"><?php the_title(); ?></h3>
	                        <h4 class="client-age"><?php echo 'Age: ' .get_post_meta( $post->ID, '_testimonial_patient_age', true ).', Work: '.get_post_meta( $post->ID, '_testimonial_patient_work', true ) . ', Place: ' . get_post_meta( $post->ID, '_testimonial_patient_place', true ); ?></h4>
	                    </div>
                     	<section class="tabHld taboutr1 section-<?php echo $post->ID; ?>">
	                        <div id="1tab_<?php echo $post->ID; ?>" class="tab-content current">
	                           <div id="examples1">
	                              <div class="inn-content mCustomScrollbar content">
	                                 <h3>Testimonials</h3>
	                                 <p><?php echo get_post_meta( $post->ID, '_testimonial_patient_testimony', true ); ?></p>
	                              </div>
	                           </div>
	                        </div>
	                        <div id="2tab_<?php echo $post->ID; ?>" class="tab-content">
	                           	<div class="inn-content mCustomScrollbar content">
	                                 <h3>Case Summary:</h3>
	                                 <?php the_content(); ?>

	                           	</div>
	                        </div>
	                        <div id="3tab_<?php echo $post->ID; ?>" class="tab-content">
	                           	<div class="inn-content mCustomScrollbar content">
	                                 <h3>Videos</h3>
                                    <?php $urls =  get_post_meta($post->ID, '_testimonial_youtube_urls', true); ?>
                                    <?php foreach ($urls as $url) { ?>
                                       <iframe src="<?php echo $url['youtube_url']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                    <?php } ?>
                                 </div>
	                        </div>
	                        <div id="4tab_<?php echo $post->ID; ?>" class="tab-content">
	                           	<div class="inn-content mCustomScrollbar content">
	                                 <h3>Images</h3>
                                    <?php $images =  get_post_meta($post->ID, '_testimonial_images', true); ?>
                                    <?php foreach ($images as $image) { ?>
                                    <?php $image_url = wp_get_attachment_image_src($image['image'], 'medium'); ?>
                                    <?php $image_url = $image_url[0]; ?>
	                                   <img src="<?php echo $image_url; ?>">
                                    <?php } ?>
	                           	</div>
	                        </div>
	                        <ul class="tabs">
	                           <li class="tab-link current" data-tab="1tab_<?php echo $post->ID; ?>"> Testimony </li>
	                           <li class="tab-link" data-tab="2tab_<?php echo $post->ID; ?>"> Case Summary </li>
	                           <li class="tab-link" data-tab="3tab_<?php echo $post->ID; ?>"> Video </li>
	                           <li class="tab-link" data-tab="4tab_<?php echo $post->ID; ?>"> Images </li>
	                        </ul>
                    	   </section>
                     
                  	</div>
               </div>
               <?php endwhile; wp_reset_postdata(); ?>
         	</div>
      	</div>
         <script src="<?php echo plugin_dir_url( __FILE__ ); ?>/js/jquery-3.1.1.js"></script>
      	<script src="<?php echo plugin_dir_url( __FILE__ ); ?>/js/jquery.mCustomScrollbar.concat.min.js"></script>
      	<script src="<?php echo plugin_dir_url( __FILE__ ); ?>/js/common-ui.js"></script>
      
   
     
   </body>



<?php get_footer(); ?>
