<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>	
<!--Services Details-->
	<div class="col-6">
      	<div class="main-content">
         	<div class="client-img">
               <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id(), 'full' ); ?>
            	<img src="<?php echo $img[0]; ?>">   
         	</div>
            <div class="inn-client-details">
                <h3 class="client-name"><?php the_title(); ?></h3>
                <h4 class="client-age"><?php echo 'Age: ' .get_post_meta( $post->ID, '_testimonial_patient_age', true ).', Work: '.get_post_meta( $post->ID, '_testimonial_patient_work', true ) . ', Place: ' . get_post_meta( $post->ID, '_testimonial_patient_place', true ); ?></h4>
            </div>
         	<section class="tabHld taboutr1 section-<?php echo $post->ID; ?>">
                <div id="1tab_<?php echo $post->ID; ?>" class="tab-content current">
                   <div id="examples1">
                      <div class="inn-content mCustomScrollbar content">
                         <h3>Testimonials</h3>
                         <p><?php echo get_post_meta( $post->ID, '_testimonial_patient_testimony', true ); ?></p>
                      </div>
                   </div>
                </div>
                <div id="2tab_<?php echo $post->ID; ?>" class="tab-content">
                   	<div class="inn-content mCustomScrollbar content">
                         <h3>Case Summary:</h3>
                         <?php the_content(); ?>

                   	</div>
                </div>
                <div id="3tab_<?php echo $post->ID; ?>" class="tab-content">
                   	<div class="inn-content mCustomScrollbar content">
                         <h3>Videos</h3>
                        <?php $urls =  get_post_meta($post->ID, '_testimonial_youtube_urls', true); ?>
                        <?php foreach ($urls as $url) { ?>
                           <iframe src="<?php echo $url['youtube_url']; ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        <?php } ?>
                     </div>
                </div>
                <div id="4tab_<?php echo $post->ID; ?>" class="tab-content">
                   	<div class="inn-content mCustomScrollbar content">
                         <h3>Images</h3>
                        <?php $images =  get_post_meta($post->ID, '_testimonial_images', true); ?>
                        <?php foreach ($images as $image) { ?>
                        <?php $image_url = wp_get_attachment_image_src($image['image'], 'medium'); ?>
                        <?php $image_url = $image_url[0]; ?>
                           <img src="<?php echo $image_url; ?>">
                        <?php } ?>
                   	</div>
                </div>
                <ul class="tabs">
                   <li class="tab-link current" data-tab="1tab_<?php echo $post->ID; ?>"> Testimony </li>
                   <li class="tab-link" data-tab="2tab_<?php echo $post->ID; ?>"> Case Summary </li>
                   <li class="tab-link" data-tab="3tab_<?php echo $post->ID; ?>"> Video </li>
                   <li class="tab-link" data-tab="4tab_<?php echo $post->ID; ?>"> Images </li>
                </ul>
        	   </section>
         
      	</div>
   </div>
<?php get_footer(); ?>
