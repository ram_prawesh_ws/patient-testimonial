<?php

/**
 * Add meta box
 *
 * @param post $post The post object
 * @link https://codex.wordpress.org/Plugin_API/Action_Reference/add_meta_boxes
 */
function testimonial_add_meta_boxes_patient_details( $post ){
	add_meta_box( 'testimonial_patient_details_meta_box', __( 'Patient Deatils', 'patient_testimonials' ), 'testimonial_build_meta_box_patient_details', 'testimonial', 'side', 'low' );
}
add_action( 'add_meta_boxes_testimonial', 'testimonial_add_meta_boxes_patient_details' );

function testimonial_add_meta_boxes_patient_testimony( $post ){
	add_meta_box( 'testimonial_patient_testimony_meta_box', __( 'Patient Testimoy', 'patient_testimonials' ), 'testimonial_build_meta_box_patient_testimony', 'testimonial', 'normal', 'low' );
}
add_action( 'add_meta_boxes_testimonial', 'testimonial_add_meta_boxes_patient_testimony' );

function testimonial_add_meta_boxes_youtube_url( $post ) {
	add_meta_box( 'testimonial_patient_youtube_url_meta_box', __( 'Youtube URL', 'patient_testimonials' ), 'testimonial_build_meta_box_youtube_url', 'testimonial', 'normal', 'low');
}
add_action('add_meta_boxes_testimonial', 'testimonial_add_meta_boxes_youtube_url');

function testimonial_add_meta_boxes_images( $post ) {
	add_meta_box( 'testimonial_patient_images_meta_box', __( 'Images', 'patient_testimonials' ), 'testimonial_build_meta_box_patient_images', 'testimonial', 'normal', 'low');
}
add_action('add_meta_boxes_testimonial', 'testimonial_add_meta_boxes_images');

function testimonial_build_meta_box_patient_testimony( $post ){
	wp_nonce_field( basename( __FILE__ ), 'testimonial_meta_box_patient_testimony_nonce' );
	$current_testimony = get_post_meta( $post->ID, '_testimonial_patient_testimony', true );
	?>
		<p>
			<!--<h3><?php //_e( 'Patient Testimony', 'patient_testimonials' ); ?></h3>-->
			<textarea name="patient_testimony"><?php echo $current_testimony; ?></textarea>
		</p>
	<?php
}
function testimonial_build_meta_box_patient_details( $post ){
	wp_nonce_field( basename( __FILE__ ), 'testimonial_meta_box_patient_details_nonce' );
	$current_age = get_post_meta( $post->ID, '_testimonial_patient_age', true );
	$current_work = get_post_meta( $post->ID, '_testimonial_patient_work', true );
	$current_place = get_post_meta( $post->ID, '_testimonial_patient_place', true );
	?>
		<p>
			<span><?php _e( 'Patient Age', 'patient_testimonials' ); ?></span>
			<input type="text" name="patient_age" value="<?php echo $current_age; ?>" /> 
		</p>
		<p>
			<span><?php _e( 'Patient Work', 'patient_testimonials' ); ?></span>
			<input type="text" name="patient_work" value="<?php echo $current_work; ?>" /> 
		</p>
		<p>
			<span><?php _e( 'Patient Place', 'patient_testimonials' ); ?></span>
			<input type="text" name="patient_place" value="<?php echo $current_place; ?>" /> 
		</p>
	<?php
}

function testimonial_build_meta_box_patient_images($post) {
 	wp_nonce_field( basename( __FILE__ ), 'testimonial_meta_box_images_nonce' );
	$current_images = get_post_meta($post->ID, '_testimonial_images', true);
	?>
	<table id="repeatable-fieldset-two-images" width="100%">
		<thead>
			<tr>
				<th width="80%"></th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if ( $current_images ) {
				$i = 0;
				foreach ( $current_images as $image ) { $i++;
					$image_url = wp_get_attachment_image_src($image['image'], 'medium');
					$image_url = $image_url[0];  ?> 
					<tr>	
						<td>
							<input name="images[]" type="hidden" class="custom_upload_image" value="<?php echo esc_attr( $image['image'] ); ?>" />
							<img src="<?php echo $image_url; ?>" class="custom_preview_image"  style="min-height: 100px;" />
						</td>
						<td>
							<a class="button custom_clear_image_button" href="javascript:void(0);">Remove Image</a>
						</td>
					</tr>
				<?php }
			}  ?>
			<!-- empty hidden one for jQuery -->
			<tr class="empty-row-image screen-reader-text-image">
				<td>
					<input hidden type="text" class="custom_upload_image" name="images[]" value="" />
					
						<img class="custom_preview_image" style="max-width:100%;" src="" />
					
				</td>
				<td>
					<a class="custom_clear_image_button button"> <?php _e( 'Remove Iamge', 'patient_testimonials' ); ?> </a>
				</td>
			</tr>
		</tbody>
	</table>
	<p><a id="add-new-image" class="button" href="javascript:void(0)">Add Image</a></p>
	<?php
} 

function testimonial_build_meta_box_youtube_url( $post ) {
	wp_nonce_field( basename( __FILE__ ), 'testimonial_meta_box_youtube_url_nonce' );
	$youtube_urls = get_post_meta($post->ID, '_testimonial_youtube_urls', true);
	?>
	<table id="repeatable-fieldset-one-youtube-url" width="100%">
		<thead>
			<tr>
				<th width="80%">URL</th>
				<th width="20%"></th>
			</tr>
		</thead>
		<tbody>
		<?php
			if ( $youtube_urls ) :
				foreach ( $youtube_urls as $youtube_url ) { ?>
					<tr>	
						<td><input type="url" class="widefat" name="youtube_urls[]" value="<?php if ($youtube_url['youtube_url'] != '') echo esc_attr( $youtube_url['youtube_url'] ); else echo ''; ?>" /></td>
					
						<td><a class="button remove-row-video" href="javascript:void(0);">Remove</a></td>
					</tr>
				<?php
				}
			else :
				// show a blank one
				?>
				<tr>
					<td><input type="url" class="widefat" name="youtube_urls[]" value="" /></td>
					<td><a class="button remove-row-video" href="javascript:void(0)">Remove</a></td>
				</tr>
			<?php endif; ?>
			<!-- empty hidden one for jQuery -->
			<tr class="empty-row-video screen-reader-text-video">
				<td><input type="url" class="widefat" name="youtube_urls[]" value="" /></td>
				<td><a class="button remove-row-video" href="javascript:void(0)">Remove</a></td>
			</tr>
		</tbody>
	</table>
	<p><a id="add-new-video" class="button" href="javascript:void(0)">Add another</a></p>
	<?php
}

function testimonial_save_meta_boxes_patient_details_data( $post_id ){
	if ( !isset( $_POST['testimonial_meta_box_patient_details_nonce'] ) || !wp_verify_nonce( $_POST['testimonial_meta_box_patient_details_nonce'], basename( __FILE__ ) ) ){
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}
	if ( isset( $_REQUEST['patient_age'] ) ) {
		update_post_meta( $post_id, '_testimonial_patient_age', sanitize_text_field( $_POST['patient_age'] ) );
	}
	if ( isset( $_REQUEST['patient_work'] ) ) {
		update_post_meta( $post_id, '_testimonial_patient_work', sanitize_text_field( $_POST['patient_work'] ) );
	}
	if ( isset( $_REQUEST['patient_place'] ) ) {
		update_post_meta( $post_id, '_testimonial_patient_place', sanitize_text_field( $_POST['patient_place'] ) );
	}
}
add_action( 'save_post_testimonial', 'testimonial_save_meta_boxes_patient_details_data', 10, 2 );

function testimonial_save_meta_boxes_patient_testimony( $post_id ){
	if ( !isset( $_POST['testimonial_meta_box_patient_details_nonce'] ) || !wp_verify_nonce( $_POST['testimonial_meta_box_patient_details_nonce'], basename( __FILE__ ) ) ){
		return;
	}
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ){
		return;
	}
	if ( ! current_user_can( 'edit_post', $post_id ) ){
		return;
	}
	if ( isset( $_REQUEST['patient_testimony'] ) ) {
		update_post_meta( $post_id, '_testimonial_patient_testimony', sanitize_text_field( $_POST['patient_testimony'] ) );
	}
}
add_action( 'save_post_testimonial', 'testimonial_save_meta_boxes_patient_testimony', 10, 2 );


function testimonial_save_meta_boxes_youtubr_url($post_id) {
	if ( ! isset( $_POST['testimonial_meta_box_youtube_url_nonce'] ) ||
	! wp_verify_nonce( $_POST['testimonial_meta_box_youtube_url_nonce'], basename( __FILE__ ) ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$current_urls = get_post_meta($post_id, '_testimonial_youtube_urls', true);
	$new = array();

	$urls = $_POST['youtube_urls'];
	$count = count( $urls );
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $urls[$i] != '' ) :
			$new[$i]['youtube_url'] = stripslashes( $urls[$i] );
		endif;
	}
	if ( !empty( $new ) && $new != $current_urls )
		update_post_meta( $post_id, '_testimonial_youtube_urls', $new );
	elseif ( empty($new) && $current_urls )
		delete_post_meta( $post_id, '_testimonial_youtube_urls', $current_urls );
}
add_action('save_post_testimonial', 'testimonial_save_meta_boxes_youtubr_url', 10, 2);

function testimonial_save_meta_boxes_images($post_id) {
	if ( ! isset( $_POST['testimonial_meta_box_images_nonce'] ) ||
	! wp_verify_nonce( $_POST['testimonial_meta_box_images_nonce'], basename( __FILE__ ) ) )
		return;
	
	if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE)
		return;
	
	if (!current_user_can('edit_post', $post_id))
		return;
	
	$current_images = get_post_meta($post_id, '_testimonial_images', true);
	$new = array();

	$images = $_POST['images'];
	$count = count( $images );
	for ( $i = 0; $i < $count; $i++ ) {
		if ( $images[$i] != '' ) :
			$new[$i]['image'] = stripslashes( $images[$i] );
		endif;
	}
	if ( !empty( $new ) && $new != $current_images )
		update_post_meta( $post_id, '_testimonial_images', $new );
	elseif ( empty($new) && $current_images )
		delete_post_meta( $post_id, '_testimonial_images', $current_images );
}
add_action('save_post_testimonial', 'testimonial_save_meta_boxes_images', 10, 2);