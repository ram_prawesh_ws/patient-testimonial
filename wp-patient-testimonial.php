<?php
/*
 Plugin Name: WP Patient Testimonial
 Plugin URI: http://euphern.com/
 Description: This plugin is developed for tesimonial to the hospital/doctor by patient with disease category wise. 
 Version: 4.X
 Author: Ram Prawesh Kumar
 Author URI: http://euphern.com/
 Text Domain: patient_testimonials
*/
 	require_once dirname(__FILE__) . '/admin-menu.php';
 	require_once dirname(__FILE__) . '/post-type.php';
 	require_once dirname(__FILE__) . '/meta-box.php';
  /**
  * custom option and settings
  */
  function patient_testimonials_settings_init() {
  	// register a new setting for "patient_testimonials" page
    register_setting( 'patient_testimonials', 'patient_testimonials_options' );
 
    // register a new section in the "patient_testimonials" page
    add_settings_section('patient_testimonials_section_developers', __( 'Testimonial display settings', 'patient_testimonials' ), 'patient_testimonials_section_developers_cb', 'patient_testimonials');
  
    add_settings_field(
    	'patient_testimonials_field_font_family', __( 'Font Family', 'patient_testimonials' ), 'patient_testimonials_field_font_family_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_font_family', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    add_settings_field(
    	'patient_testimonials_field_font_weight', __( 'Font weight', 'patient_testimonials' ), 'patient_testimonials_field_font_weight_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_font_weight', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    add_settings_field(
    	'patient_testimonials_field_bg_color', __( 'BackGround Color', 'patient_testimonials' ), 'patient_testimonials_field_bg_color_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_bg_color', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    add_settings_field(
    	'patient_testimonials_field_bg_image', __( 'BackGround Image', 'patient_testimonials' ), 'patient_testimonials_field_bg_image_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_bg_image', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    //add_settings_field(
    //	'patient_testimonials_field_bg_image_preview',  __( 'BackGround Image Preview', 'patient_testimonials' ), 'patient_testimonials_field_bg_image_preview_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_bg_image_preview', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    //);
    add_settings_field(
    	'patient_testimonials_field_bg_opacity', __( 'BackGround Opacity', 'patient_testimonials' ), 'patient_testimonials_field_bg_opacity_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_bg_opacity', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    add_settings_field(
    	'patient_testimonials_field_page_row', __( 'Testimonial row per page', 'patient_testimonials' ), 'patient_testimonials_field_page_row_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_page_row', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
    add_settings_field(
    	'patient_testimonials_field_page_column', __( 'Testimonial row per column', 'patient_testimonials' ), 'patient_testimonials_field_page_column_cb', 'patient_testimonials', 'patient_testimonials_section_developers', ['label_for' => 'patient_testimonials_field_page_column', 'class' => 'patient_testimonials_row', 'patient_testimonials_custom_data' => 'custom',]
    );
}

/**
 * register our patient_testimonials_settings_init to the admin_init action hook
 */
function patient_testimonials_get_default_options() {
    $options = array(
        'patient_testimonials_field_bg_image' => ''
    );
    return $options;
}
add_action( 'admin_init', 'patient_testimonials_settings_init' );
add_action( 'admin_enqueue_scripts', 'patient_testimonials_admin_enqueue_scripts' );
function patient_testimonials_admin_enqueue_scripts($hook) { //echo '<pre>';  print_r(get_current_screen()); echo '</pre>';
    wp_enqueue_script( 'wp-color-picker' );
    wp_enqueue_script( 'cp_custom', plugins_url( 'js/cp-custom-script.min.js', __FILE__ ), array( 'jquery', 'wp-color-picker' ), '1.1', true );
    wp_register_script( 'upload_custom', plugins_url( 'js/uploader-custom-script.js', __FILE__ ), array('jquery','media-upload','thickbox') ); 
    wp_register_script( 'repeater_custom', plugins_url( 'js/repeater-custom-script.js', __FILE__ ), array('jquery') ); 
    wp_register_script( 'repeater_uploader_custom', plugins_url( 'js/repeater-uploader.js', __FILE__ ), array('jquery','media-upload','thickbox') ); 
    if ( 'toplevel_page_manage-testimonials' == get_current_screen() -> id ) {
        wp_enqueue_script('jquery');

        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
 
        wp_enqueue_script('media-upload');
        wp_enqueue_script('upload_custom');
        wp_enqueue_script( 'repeater_custom');
 		wp_enqueue_style( 'wp-color-picker');
    }
    if ( 'testimonial' == get_current_screen() -> id ) {
        wp_enqueue_script('jquery');
        wp_enqueue_script('thickbox');
        wp_enqueue_style('thickbox');
 
        wp_enqueue_script('media-upload');
        wp_enqueue_script( 'repeater_custom');
        wp_enqueue_script( 'repeater_uploader_custom');
	}
}
/**
 * custom option and settings:
 * callback functions
 */
 
// developers section cb
 
// section callbacks can accept an $args parameter, which is an array.
// $args have the following keys defined: title, id, callback.
// the values are defined at the add_settings_section() function.
function patient_testimonials_section_developers_cb( $args ) { ?>
 	<p id="<?php echo esc_attr( $args['id'] ); ?>"><?php esc_html_e( 'Set the below settings.', 'patient_testimonials' ); ?></p>
<?php }
// pill field cb
 
// field callbacks can accept an $args parameter, which is an array.
// $args is defined at the add_settings_field() function.
// wordpress has magic interaction with the following keys: label_for, class.
// the "label_for" key value is used for the "for" attribute of the <label>.
// the "class" key value is used for the "class" attribute of the <tr> containing the field.
// you can add custom key value pairs to be used inside your callbacks.
function patient_testimonials_field_pill_cb( $args ) {
  // get the value of the setting we've registered with register_setting()
  $options = get_option( 'patient_testimonials_options' );
  // output the field
  ?>
  <select id="<?php echo esc_attr( $args['label_for'] ); ?>"
   data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>"
   name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]"
  >
  <option value="red" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'red', false ) ) : ( '' ); ?>>
  <?php esc_html_e( 'red pill', 'patient_testimonials' ); ?>
  </option>
  <option value="blue" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'blue', false ) ) : ( '' ); ?>>
  <?php esc_html_e( 'blue pill', 'patient_testimonials' ); ?>
  </option>
  </select>
  <p class="description">
  <?php esc_html_e( 'You take the blue pill and the story ends. You wake in your bed and you believe whatever you want to believe.', 'patient_testimonials' ); ?>
  </p>
  <p class="description">
  <?php esc_html_e( 'You take the red pill and you stay in Wonderland and I show you how deep the rabbit-hole goes.', 'patient_testimonials' ); ?>
  </p>
  <?php
}
function patient_testimonials_field_font_family_cb( $args ) {
  	
  	$options = get_option( 'patient_testimonials_options' );
  	/*
  	$fontFile = plugins_url('/cache/google-web-fonts.txt', __FILE__ );
        //Total time the file will be cached in seconds, set to a week
	$cachetime = 86400 * 7;
	if(file_exists($fontFile) && $cachetime < filemtime($fontFile)) {
	    $content = json_decode(file_get_contents($fontFile));
	} else {
	    $googleApi = 'https://www.googleapis.com/webfonts/v1/webfonts?sort=popularity&key={AIzaSyB1QJsnGG3BuDhB3We0n_cGFCHZ1ouocLw}';
	    $fontContent = wp_remote_get( $googleApi, array('sslverify'   => false) );
	    //print_r($fontContent); exit();
	    $fp = fopen($fontFile, 'w');
	    fwrite($fp, $fontContent['body']);
	    fclose($fp);
	    $content = json_decode($fontContent['body']);
	} //echo '<pre>'; print_r($content); exit();
	*/


  ?>
  	<select id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" >
   		<option value="ABeeZee" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'ABeeZee', false ) ) : ( '' ); ?>>
    		<?php esc_html_e( 'ABeeZee', 'patient_testimonials' ); ?>
   		</option>
	   <option value="Abhaya Libre" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Abhaya Libre', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Abhaya Libre', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Abril Fatface" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Abril Fatface', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Abril Fatface', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Aclonica" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Aclonica', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Aclonica', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Acme" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Acme', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Acme', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Actor" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Actor', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Actor', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Adamina" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Adamina', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Adamina', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Advent Pro" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Advent Pro', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Advent Pro', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Aguafina Script" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Aguafina Script', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Aguafina Script', 'patient_testimonials' ); ?>
	   </option>
	</select>
  	<p class="description">
   		<?php esc_html_e( 'Choose font family for display testimonial.', 'patient_testimonials' ); ?>
  	</p>
  	<?php
}
function patient_testimonials_field_font_weight_cb( $args ) {
  	// get the value of the setting we've registered with register_setting()
  	$options = get_option( 'patient_testimonials_options' );
  	// output the field
  	?>
  	<select id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" >
	   <option value="Base" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Base', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Base', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Medium" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Medium', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Medium', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Bold" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Bold', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Bold', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Light" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Light', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Light', 'patient_testimonials' ); ?>
	   </option>
	   <option value="Ultra Light" <?php echo isset( $options[ $args['label_for'] ] ) ? ( selected( $options[ $args['label_for'] ], 'Ultra Light', false ) ) : ( '' ); ?>>
	    <?php esc_html_e( 'Ultra Light', 'patient_testimonials' ); ?>
	   </option>
  	</select>
  	<p class="description">
   		<?php esc_html_e( 'Choose font weight for display testimonial.', 'patient_testimonials' ); ?>
  	</p>
  <?php
}
function patient_testimonials_field_bg_color_cb( $args ) {
	// get the value of the setting we've registered with register_setting()
	$options = get_option( 'patient_testimonials_options' );
	// output the field
	if(isset( $options[ $args['label_for'] ] )){
		echo $options[ $args['label_for'] ];
	} ?>
	<input name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" id="<?php echo esc_attr( $args['label_for'] ); ?>" type="text" value="<?php  if ( isset( $options[ $args['label_for'] ] ) ) echo $options[ $args['label_for'] ]; ?>" />
	<?php
}
function patient_testimonials_field_bg_image_cb( $args ) {
  	// get the value of the setting we've registered with register_setting()
  	$options = get_option( 'patient_testimonials_options' );
  	// output the field
  	if(isset( $options[ $args['label_for'] ] )){
  		//echo $options[ $args['label_for'] ];
  	} ?>
  	<input type="text" id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php  if ( isset( $options[ $args['label_for'] ] ) ) echo $options[ $args['label_for'] ]; ?>" />
    <input id="upload_BG_button" type="button" class="button" value="<?php _e( 'Upload BackGround Iamge', 'patient_testimonials' ); ?>" />
    <span class="description">
    	<?php esc_html_e('Upload an image for the BackGround image.', 'patient_testimonials' ); ?>
    </span>
    <?php if (isset($options[ $args['label_for'] ])) { ?>
	    <div class="bg_preview" style="min-height: 100px;">
	        <img id="bg_preview" style="max-width:100%;" src="<?php echo esc_url( $options[ $args['label_for'] ] ); ?>" />
	    </div>
  <?php }
}
/*function patient_testimonials_field_bg_image_preview_cb ($args) {
	$options = get_option( 'patient_testimonials_options' );  ?>
    <div id="<?php echo esc_attr( $args['label_for'] ); ?>" style="min-height: 100px;">
        <img style="max-width:100%;" src="<?php echo esc_url( $options[ $args['label_for'] ] ); ?>" />
    </div>
    <?php
}*/
function patient_testimonials_field_bg_opacity_cb( $args ) {
  	// get the value of the setting we've registered with register_setting()
  	$options = get_option( 'patient_testimonials_options' );
  	// output the field
  	?>
  	<input id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for']]; ?>">
  	<p class="description">
   		<?php esc_html_e( 'Enter opacity for display', 'patient_testimonials' ); ?>
  	</p>
  <?php
}
function patient_testimonials_field_page_row_cb( $args ) {
  	// get the value of the setting we've registered with register_setting()
  	$options = get_option( 'patient_testimonials_options' );
  	// output the field
  	?>
  	<input id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for']]; ?>">
  	<p class="description">
   		<?php esc_html_e( 'Enter number of testimonial row dislpay per page', 'patient_testimonials' ); ?>
  	</p>
  <?php
}
function patient_testimonials_field_page_column_cb( $args ) {
  	// get the value of the setting we've registered with register_setting()
  	$options = get_option( 'patient_testimonials_options' );
  	// output the field
  	?>
  	<input id="<?php echo esc_attr( $args['label_for'] ); ?>" data-custom="<?php echo esc_attr( $args['patient_testimonials_custom_data'] ); ?>" name="patient_testimonials_options[<?php echo esc_attr( $args['label_for'] ); ?>]" value="<?php echo $options[ $args['label_for']]; ?>">
  	<p class="description">
  		<?php esc_html_e( 'Enter number of testimonial column dislpay per page.', 'patient_testimonials' ); ?>
  	</p>
  	<?php
}


/**
 * top level menu:
 * callback functions
 */
function patient_testimonials_options_page_html() {
  // check user capabilities
  if ( ! current_user_can( 'manage_options' ) ) {
    return;
  }
 
  // add error/update messages
 
  // check if the user have submitted the settings
  // wordpress will add the "settings-updated" $_GET parameter to the url
   if ( isset( $_GET['settings-updated'] ) ) {
    // add settings saved message with the class of "updated"
    add_settings_error( 'patient_testimonials_messages', 'patient_testimonials_message', __( 'Settings Saved', 'patient_testimonials' ), 'updated' );
   }
 
   // show error/update messages
   settings_errors( 'patient_testimonials_messages' );
   ?>
   <div class="wrap">
     <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
     <form action="options.php" method="post">
      <?php
       // output security fields for the registered setting "wporg"
       settings_fields( 'patient_testimonials' );
       // output setting sections and their fields
       // (sections are registered for "wporg", each field is registered to a specific section)
       do_settings_sections( 'patient_testimonials' );
       // output save settings button
       submit_button( 'Save Settings' );
      ?>
     </form>
  </div>
 <?php
}