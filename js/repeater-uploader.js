jQuery(function(jQuery) {
    jQuery( '.empty-row-image' ).attr('style','display:none');
    jQuery('.custom_upload_image_button').click(function() {
		//alert(jQuery(this).parents().find('.custom_upload_image').val());
		imageRow =	jQuery(this).parents().find('.custom_upload_image');
		previewRow = jQuery(this).parents().find('.custom_preview_image'); 
        tb_show('', 'media-upload.php?type=image&TB_iframe=true');
        window.send_to_editor = function(html) {
            imgurl = jQuery(html).attr('src');
            classes = jQuery(html).attr('class');
            id = classes.replace(/(.*?)wp-image-/, '');
            imageRow.val(id);
            previewRow.attr('src', imgurl);
            tb_remove();
        }
		jQuery(this).addClass('custom_clear_image_button');		
		jQuery(this).removeClass('custom_upload_image_button');
		jQuery(this).text('Remove Image');
        return false;
    });
     
    jQuery('.custom_clear_image_button').click(function() {
        jQuery(this).parents('tr').remove();
		return false;
    });

	jQuery( '#add-new-image' ).on('click', function() {
		var row = jQuery( '.empty-row-image.screen-reader-text-image' ).clone(true);
		row.removeClass( 'empty-row-image screen-reader-text-image' );
		
        tb_show('', 'media-upload.php?type=image&TB_iframe=true');
        window.send_to_editor = function(html) {
            imgurl = jQuery(html).attr('src');
            classes = jQuery(html).attr('class');
            id = classes.replace(/(.*?)wp-image-/, '');
			row.find('.custom_upload_image').val(id);
			row.find('.custom_preview_image').attr('src', imgurl);
            //imageRow.val(id);
            //previewRow.attr('src', imgurl);
            tb_remove();
        }
		row.removeAttr('style','display:block');
		row.insertBefore( '#repeatable-fieldset-two-images tbody>tr:last' );
		return false;
	});
});
