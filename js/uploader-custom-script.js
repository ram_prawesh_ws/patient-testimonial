jQuery(document).ready(function($) {
    $('#upload_BG_button').click(function() {
        tb_show('Upload a logo', 'media-upload.php?referer=patient_testimonials&type=image&TB_iframe=true&post_id=0', false);
        return false;
    });
	window.send_to_editor = function(html) { //console.log(html);
		//var image_url = $('img',html).attr('src');
		var image_url = $(html).attr('src');
		 //console.log(image_url);
		$('#patient_testimonials_field_bg_image').val(image_url);
		$('#bg_preview').attr('src',image_url);
		tb_remove();
	}
});
