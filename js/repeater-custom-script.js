jQuery(document).ready(function( $ ){
	$( '.empty-row-video' ).attr('style','display:none');
	$( '#add-new-video' ).on('click', function() {
		var row = $( '.empty-row-video.screen-reader-text-video' ).clone(true);
		row.removeClass( 'empty-row-video screen-reader-text-video' );
		row.removeAttr('style','display:block');
		row.insertBefore( '#repeatable-fieldset-one-youtube-url tbody>tr:last' );
		return false;
	});

	$( '.remove-row-video' ).on('click', function() {
		$(this).parents('tr').remove();
		return false;
	});
});
