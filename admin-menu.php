<?php
	if ( ! function_exists( 'patient_testimonials_options_page' ) && ! function_exists( 'patient_testimonials_options_page' ) ) {

		function patient_testimonials_options_page() {
			# Settings for custom testimonial menu
	        $page_title = 'Testimonials';
	        $menu_title = 'Manage Testimonials';
	        $capability = 'manage_options';
	        $menu_slug  = 'manage-testimonials';
	        $function   = 'patient_testimonials_options_page_html';// Callback function which displays the page content.
	        $icon_url   = 'dashicons-admin-page';
	        $position   = 0;

	        # Add custom admin menu
        	add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );

        	$submenu_pages = array(
            	/*# Avoid duplicate pages. Add submenu page with same slug as parent slug.
	            array(
	                'parent_slug' => 'manage-testimonials',
	                'page_title'  => 'Testimonials',
	                'menu_title'  => 'Manage Testimonials',
	                'capability'  => 'read',
	                'menu_slug'   => 'manage-testimonials',
	                'function'    => 'testimonial_admin_page',// Uses the same callback function as parent menu.
	            ),*/

	            # Post Testimonials :: View All Testimonials
	            array(
	                'parent_slug' => 'manage-testimonials',
	                'page_title'  => 'All Testimonials',
	                'menu_title'  => 'All Testimonials',
	                'capability'  => 'edit_posts',
	                'menu_slug'   => 'edit.php?post_type=testimonial',
	                'function'    => null,// Doesn't need a callback function.
	            ),

	            # Post Testimonials :: Add New Testimonials
	            array(
	                'parent_slug' => 'manage-testimonials',
	                'page_title'  => 'Add Testimonial',
	                'menu_title'  => 'Add Testimonial',
	                'capability'  => 'edit_posts',
	                'menu_slug'   => 'post-new.php?post_type=testimonial',
	                'function'    => null,// Doesn't need a callback function.
	            ),

	            # Taxonomy :: Manage Testimonials Categories
	            array(
	                'parent_slug' => 'manage-testimonials',
	                'page_title'  => 'Desies Categories',
	                'menu_title'  => 'Diseas Categories',
	                'capability'  => 'edit_posts',
	                'menu_slug'   => 'edit-tags.php?taxonomy=disease-categories&post_type=testimonial',
	                'function'    => null,// Doesn't need a callback function.
	            ),

	            /*# Settings :: Manage Testimonials Display Settings
	            array(
	                'parent_slug' => 'manage-testimonials',
	                'page_title'  => 'Testimonials Settings',
	                'menu_title'  => 'Testimonials Settings',
	                'capability'  => 'manage_options',
	                'menu_slug'   => 'testimonial-settings',
	                'function'    => 'patient_testimonials_options_page_html'
	            ),*/

	        );

	        # Add each submenu item to custom admin menu.
	        foreach ( $submenu_pages as $submenu ) {

	            add_submenu_page(
	                $submenu['parent_slug'],
	                $submenu['page_title'],
	                $submenu['menu_title'],
	                $submenu['capability'],
	                $submenu['menu_slug'],
	                $submenu['function']
	            );

	        }
		}
		add_action( 'admin_menu', 'patient_testimonials_options_page', 1 );

		# Default Admin Page for Testimonial Admin Menu
	    function testimonial_admin_page() {

	        # Display Testimonial admin page content from newly added Testimonial admin menu.
	        echo '<div class="wrap">' . PHP_EOL;
	        echo '<h2>Testimonials</h2>' . PHP_EOL;
	        echo '<p>This is the custom admin page created from the custom admin menu.</p>' . PHP_EOL;
	        echo '</div><!-- end .wrap -->' . PHP_EOL;
	        echo '<div class="clear"></div>' . PHP_EOL;

	    }

	}

	if ( ! function_exists( 'testimonials_current_menu' ) ) {

	    function testimonials_current_menu( $parent_file ) {
	        global $submenu_file, $current_screen, $pagenow;

	        # Set the submenu as active/current while anywhere in your testimonials
	        if ( $current_screen->post_type == 'testimonial' ) {

	            if ( $pagenow == 'post.php' ) {
	                $submenu_file = 'edit.php?post_type=' . $current_screen->post_type;
	            }

	            if ( $pagenow == 'edit-tags.php' ) {
	                $submenu_file = 'edit-tags.php?taxonomy=disease-categories&post_type=' . $current_screen->post_type;
	            }

	            $parent_file = 'manage-testimonials';

	        }

	        return $parent_file;

	    }

	    //add_filter( 'parent_file', 'testimonials_current_menu' );

	}
?>